from django.shortcuts import render
from urllib.parse import urlparse
from shop.models import *
from itertools import chain
import random

def home(request):
    product_reccomandation= None
    last_prod = None
    selled_product = None

    orders = Order.objects.all().filter(customer_id=request.user.id)
    items = Product.objects.all().filter(category_id__in=Category.objects.all())[6:9]
    if request.user.is_authenticated:
        selled_product = Order.objects.filter(product__in = Product.objects.all().filter(seller_id = request.user.id).distinct())
        product_reccomandation=reccomandation_system(request)
        last_prod = last_visited(request.META.get('HTTP_REFERER'))
    ctx = {
        'new_products':items,
        'orders':orders,
        'random_product': random_product(),
        'last_prod':last_prod,
        'most_selled':Order.objects.all().order_by("product")[0],
        'product_reccomandation': product_reccomandation,
        'selled_product' : selled_product,
        'categories': Category.objects.all(),
        }
        
    return render(request, template_name='base.html',context=ctx)

# Calcolo in maniera randomica il prodotto per la funzionalità "lasciati ispirare"
def random_product():
    tot = len(Product.objects.all())
    return Product.objects.filter(id = random.randint(2,tot+2))

# Prendo l'ultimo prodotto visualizzato nel dettagli facendo una semplice operazione di parsing 
def last_visited(lastPage):
    if lastPage is None:
        return
    path = urlparse(lastPage).path
    splitted_path = list(filter(None,str.split(path,"/")))
    length =len(splitted_path)
    if  length != 3 or splitted_path[0] != "shop" or splitted_path[length-1] != "details":
        return
    print(splitted_path[1])
    parsed_title = splitted_path[1].replace("-"," ").title()
    print(parsed_title)
    last_prod =  Product.objects.filter(category_id__in = Product.objects.all().filter(title = parsed_title).values("category"))
    last_seller_prod = Product.objects.filter(seller__in= Product.objects.all().filter(title = parsed_title).values("seller"))
    product = list(chain(last_prod, last_seller_prod))
    return product

def reccomandation_system(request):
    product = Product.objects.none()
    THERESOLD = 3
    # Prendo i prodotti che ho acquistato escluso quelli 
    for order in Order.objects.all().filter(customer_id=request.user.id):
        product = Product.objects.filter(category_id=order.product.category).exclude(id=order.product_id)
    
    # Prendo il la categoria del prodotto al quale ho messo le stelle maggiori di una certa soglia 
    reviews = ReviewProduct.objects.filter(reviewer_id = request.user.id, stars__gte = THERESOLD)
    reviews_recc = Product.objects.filter(id__in = reviews.values("product")).exclude(id__in = Order.objects.all().filter(customer_id=request.user.id).values("product"))
    
    product_reviews_recc = Product.objects.filter(category__in = reviews_recc.values("category"))

    # Faccio il merge dei due Queryset 
    product = list(chain(product, product_reviews_recc))
    return product