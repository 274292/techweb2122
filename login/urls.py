from django.urls import path
from .views import *


app_name = 'login'

urlpatterns = [
    path('registration/', UserCreateView.as_view(), name="registration"),
    path('login/', UserLogin.as_view(), name="login"),
    path('logout/', UserLogout.as_view(), name="logout"),
    path('userinfo/', UserDetails.as_view(), name='userdetails'),
    path('dashboard/<int:pk>/', DashboardSeller.as_view(), name='dashboard'),
    path('yourproduct/', ProductSeller.as_view(), name='yourproduct'),
    path('yourorder/', OrderList.as_view(), name='yourorder')
]