from django.shortcuts import render
from django.contrib.auth import login, authenticate
from django.contrib.auth.mixins import LoginRequiredMixin
from braces.views import GroupRequiredMixin
from django.contrib import messages
from django.contrib.auth.models import User, Group
from login.forms import CustomUserForm
from django.views.generic.edit import CreateView
from django.views.generic import ListView, DetailView
from django.contrib.auth.views import LoginView, LogoutView
from shop.models import *
from django.urls import reverse_lazy
# Create your views here.

class UserCreateView(CreateView):
    form_class = CustomUserForm
    template_name = 'login/user_creation.html'
    success_url = reverse_lazy("homepage")

    def form_valid(self,form):
        valid = super().form_valid(form)
        user = form.save(commit=False)
        user.save()

        if form.cleaned_data['is_seller']:
            user_group = Group.objects.get(name="seller")
            user.groups.add(user_group)
        
        user = authenticate(
            username = form.cleaned_data['username'],
            password = form.cleaned_data['password1'],
        )
        login(self.request, user)

        return valid

class UserLogin(LoginView):
    template_name = 'login/login.html'
    next_page = reverse_lazy('homepage')

class UserLogout(LogoutView):
    template_name = 'login/logout.html'
    next_page = reverse_lazy('homepage')

class UserDetails(LoginRequiredMixin, DetailView):
    model = User
    template_name = 'login/info_user.html'
   
    def get_object(self):
        return self.request.user
    def get_context_data(self, **kwargs):
        tot=0
        context = super().get_context_data(**kwargs)
        num_orders = Order.objects.filter(customer_id = self.request.user)
        context['num_orders'] = len(num_orders)
        for order in num_orders:
            tot += order.product.price
        context['sum_orders'] = tot
        
        return context

class ProductSeller(GroupRequiredMixin, ListView):

    raise_exception = True
    group_required = ["seller"]
    model = Product
    template_name = 'login/product_seller.html'

    def get_queryset(self):
        return Product.objects.all().filter(seller=self.request.user)

class DashboardSeller(GroupRequiredMixin, DetailView):

    raise_exception = True
    group_required = ["seller"]
    model = User
    template_name = 'login/dashboard_seller.html'
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['my_review'] = None
        num_reviews = 0
        tot = 0
        total_score = 0


        # REVENUE
        orders =Order.objects.filter(product_id__in=Product.objects.filter(seller=self.get_object().id).values_list('id',flat=True)) 
        context["total_sold_products"] = len(orders)
        for order in orders:
            tot += order.product.price
            context["total_sold_revenue"] = tot

        context["orders"] = orders

        # RATING
        total_seller_reviews = ReviewSeller.objects.filter(seller_id=self.get_object().id)
        context['total_seller_reviews'] = len(total_seller_reviews)

        for review in ReviewSeller.objects.filter(seller_id=self.get_object().id):
            num_reviews += 1
            total_score += review.stars
        try:
            context['average_rating'] = round(float(total_score/num_reviews),2)
        except:
            context['average_rating'] = 0
        
        # REVIEW
        try:
            if self.request.user.is_authenticated:
                seller_id = self.get_object().id
                user_id = User.objects.get(username=self.request.user).id
                review = ReviewSeller.objects.all().filter(seller_id =seller_id)

                
                if review:
                    context['my_review'] = review
        except:
            pass

        return context


class OrderList(LoginRequiredMixin,ListView):
    model = Order
    template_name = 'login/yourorder.html'
    def get_queryset(self):
        return Order.objects.filter(customer=self.request.user)