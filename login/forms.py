from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.forms import fields

class CustomUserForm(UserCreationForm):
    is_seller = fields.BooleanField(label='I am a seller', required=False)

    class Meta:
        model = User
        fields = ['username','email','password1', 'password2']