from django.test import TestCase
from django.urls import reverse
from django.contrib.auth.models import User, Group
from shop.models import Product,Category
# Create your tests here.

class AnonymousUserViewTest(TestCase):
    # Testing if anonymus user can access to userdetails page
    def test_access_to_user_detail(self):
        response = self.client.get(reverse('login:userdetails'))
        self.assertEqual(response.status_code, 302)
    # Testing if anonymus user can access to yourorder page
    def test_access_to_yourorder(self):
        response = self.client.get(reverse('login:yourorder'))
        self.assertEqual(response.status_code, 302)
    
    # def test_add_review(self):
    #     test = Product.objects.create(
    #     category= Category.objects.create(title="test",slug="django"),
    #     title="test",
    #     price=10,
    #     seller=User.objects.create(username="test",password="pass"))
    #     response = self.client.get(reverse('shop:review_product',args=(test.slug,)))
    #     print(response)

class PurchaserUserViewTest(TestCase):
    def setUp(self):
        self.usertest = User.objects.create_user(username='test', password='pass')

    def login(self):
        login = self.client.login(username='test', password='pass')
        return login
    
    # Test login
    def test_login(self):
        self.assertEqual(self.login(), True)


    # Testing if purchaser user can access to yourorder page
    def test_access_to_dashboard(self):
        self.login()
        response = self.client.get(reverse('login:dashboard',kwargs={'pk':self.usertest.pk}))
        self.assertEqual(response.status_code, 403)

    # Testing if purchaser user can access to userdetails page
    def test_access_to_user_detail(self):
        self.login()
        response = self.client.get(reverse('login:userdetails'))
        self.assertEqual(response.status_code, 200)

     # Testing if purchaser user can access to yourproduct page
    def test_access_to_yourproduct(self):
        self.login()
        response = self.client.get(reverse('login:yourproduct'))
        self.assertEqual(response.status_code, 403)
    
    # Testing if purchaser user can insert a new product 
    def test_insert_new_product(self):
        self.login()
        responses = [
            self.client.get(reverse('shop:create')),
            self.client.post(reverse('shop:create'))
        ]
        for response in responses:
            self.assertEqual(response.status_code, 403)

class SellerUserViewTest(TestCase):
    def setUp(self):
        self.usertest = User.objects.create_user(username='test', password='pass')
        seller_group = Group.objects.create(name="seller")
        self.usertest.groups.add(seller_group)
    def login(self):
        login = self.client.login(username='test', password='pass')
        return login
    
    # Test login
    def test_login(self):
        self.assertEqual(self.login(), True)

    # Testing if seller user can insert a new product 
    def test_insert_new_product(self):
        self.login()
        responses = [
            self.client.get(reverse('shop:create')),
            self.client.post(reverse('shop:create'))
        ]
        for response in responses:
            self.assertEqual(response.status_code, 200)
