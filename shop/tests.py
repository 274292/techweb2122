from django.test import TestCase
from django.urls import reverse
from django.contrib.auth.models import User, Group
from .models import Category,Product
# Create your tests here.

class ShopTestView(TestCase):


    # SEARCHING & FILTERS
    # Testing if searcher works with integer input
    def test_search(self):
        title_int = 2
        response = self.client.get(reverse("shop:search"),data={"title_contains":title_int})
        self.assertEqual(response.status_code,200)

    # Testing if price filter works with string input
    def test_filter_price(self):
        range_min = "1"
        range_max = "100"
        response = self.client.get(reverse("shop:price_filter"), data={"min_price":range_min,"max_price":range_max})
        self.assertEqual(response.status_code,200)

    # Testing if category filter works
    def test_category_filter(self):
        test = Category.objects.create(title="test",slug="django")
        response = self.client.get(reverse('shop:category_filter', args=(test.slug,)))
        self.assertEqual(response.status_code,200)