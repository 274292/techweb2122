from django.urls import path
from .views import *


app_name = 'shop'

urlpatterns = [
    path('items/', ProductsListView.as_view(), name="items"),
    path('<slug:slug>/details/', ProductDetails.as_view(), name="itemdetails"),
    path('<slug:slug>/buyitem', BuyProduct.as_view(), name='buyitem'),
    path('<slug:slug>/addorder/', add_order, name="addorder"),
    path('search/', search, name="search"),
    path('size_filter/', size_filter, name="size_filter"),
    path('price_filter/', price_filter, name="price_filter"),
    #path('categories/', CategoryListView.as_view(), name='categories'),
    path('category/<slug:slug>/list', category_filter, name='category_filter'),
    path('create_product/', ProductSeller_Create.as_view(), name='create'),
    path('<slug:slug>/delete', ProductSeller_Delete.as_view(), name='delete'),
    path('<slug:slug>/update', ProductSeller_Update.as_view(), name='update'),
    path('<slug:slug>/review', review, name='review_product'),
    path('<int:pk>/review_seller', review_seller, name='review_seller'),
    path('seller_details/<int:pk>/', SellerDetails.as_view(), name='sellerdetails'),

    
]