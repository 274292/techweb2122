from django.shortcuts import render,redirect
from django.utils import timezone
from django.views.generic.list import ListView
from braces.views import GroupRequiredMixin
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import DetailView,CreateView,DeleteView,UpdateView
from django.urls import reverse_lazy
from .forms import *
from .models import *
from django.db.models import Q

# Create your views here.

class ProductSeller_Create(GroupRequiredMixin, CreateView):
    raise_exception = True
    group_required = ["seller"]
    form_class = ProductForm
    model = Product
    template_name = 'shop/create_product.html'
    success_url = reverse_lazy('login:yourproduct')

    def form_valid(self, form):
        form.instance.seller_id = self.request.user.id
        return super().form_valid(form)

class ProductSeller_Delete(GroupRequiredMixin, DeleteView):
    raise_exception = True
    group_required = ["seller"]
    model = Product
    template_name = 'shop/delete_product.html'
    success_url = reverse_lazy('login:yourproduct')

class ProductSeller_Update(GroupRequiredMixin, UpdateView):
    raise_exception = True
    group_required = ["seller"]
    form_class = ProductForm
    model = Product
    template_name = 'shop/update_product.html'
    success_url = reverse_lazy('login:yourproduct')



class ProductsListView(ListView):
    model = Product
    template_name = 'shop/item_list.html'

    def get_context_data(self, **kwargs):
        # Categorie
        context = super().get_context_data(**kwargs)
        context['categories'] = Category.objects.all()
        return context


class BuyProduct(LoginRequiredMixin, DetailView):
    success_url = reverse_lazy('shop:itemdetails')
    template_name = 'shop/buy_product.html'
    model = Product

def add_order(request,slug):
    order = Order(
            customer = User.objects.get(id=request.user.id),
            product = Product.objects.get(slug=slug),
            date = timezone.now(),
        )
    order.save()
    return redirect('shop:itemdetails', slug)

class ProductDetails(DetailView):
    model = Product
    template_name = 'shop/item_details.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        
        # Categoria
        rs_category = Product.objects.filter(category_id=self.get_object().category_id).exclude(id=self.get_object().id)
        context['rs_category'] = rs_category
        # Prezzo
        price = self.get_object().price
        bias = 30
        rs_price = Product.objects.filter(price__gte=price-bias,price__lte=price+bias).exclude(id=self.get_object().id).exclude(category_id=self.get_object().category_id)

        context['rs_price'] = rs_price

        context['already_reviewed'] = False
        context['my_review'] = None
        try:
            if self.request.user.is_authenticated:
                product = self.get_object().id
                user_id = User.objects.get(username=self.request.user).id
                review = ReviewProduct.objects.get(product_id=product, reviewer_id=user_id)

                if review:
                    context['my_review'] = review
                    context['already_reviewed'] = True
        except:
            pass

        return context

class SellerDetails(DetailView):
    model = User
    template_name = 'shop/seller_details.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['self_review'] = False
        context['already_reviewed'] = False
        context['my_review'] = None
        
        num_reviews = 0
        total_score = 0
        context['seller_products'] = Product.objects.filter(seller_id = self.get_object().id)
        total_seller_reviews = ReviewSeller.objects.filter(seller_id=self.get_object().id)
        context['total_seller_reviews'] = len(total_seller_reviews)

        for review in ReviewSeller.objects.filter(seller_id=self.get_object().id):
            num_reviews += 1
            total_score += review.stars
        try:
            context['average_rating'] = round(float(total_score/num_reviews),2)
        except:
            context['average_rating'] = 0

        try:
            if self.request.user.is_authenticated:
                seller_id = self.get_object().id
                #user_id = User.objects.get(username=self.request.user).id
                review = ReviewSeller.objects.all().filter(seller_id =seller_id )

                
                if review:
                    context['my_review'] = review
                    context['already_reviewed'] = True
        except:
            pass

        try:
            if self.request.user.is_authenticated:
                seller_id = self.get_object().id
                reviewer_id = User.objects.get(username=self.request.user).id
                if seller_id == reviewer_id:
                    context['self_review'] = True
        except:
            pass

        return context 


def review(request, slug):
    template = 'shop/reviews.html'
    ctx = {
        "form": ProductReviewForm(),
        "seller": None,
        "product": slug
    }

    if request.method == "POST":
        form = ProductReviewForm(request.POST)

        if form.is_valid():
            review = ReviewProduct()
            review.stars = form.cleaned_data.get('review_value')
            review.text = form.cleaned_data.get('review_text')
            review.product = Product.objects.get(slug=slug)
            review.reviewer = User.objects.get(username=request.user.username)
            
            review.save()
            return redirect("shop:itemdetails", slug)

    return render(request, template_name=template, context=ctx)




#  Reviewer del venditore
def review_seller(request, pk):
    template = 'shop/reviews.html'
    ctx = {
        'form': SellerReviewForm(),
        'vendor': pk
    }

    if request.method == "POST":
        form = SellerReviewForm(request.POST)

        if form.is_valid():
            review = ReviewSeller()
            review.stars = form.cleaned_data.get('review_value')
            review.reviewer = User.objects.get(username=request.user.username)
            review.seller = User.objects.get(id=pk)
            review.save()
            return redirect("shop:sellerdetails", pk)

    return render(request, template_name=template, context=ctx)


def is_valid_queryparam(param):
    return param != '' and param is not None



# Searching
def search(request):
    products = Product.objects.all()
    query = (str)(request.GET.get('title_contains'))
    if is_valid_queryparam(query):
        products = products.filter(Q(title__icontains=query) | Q(category__title__icontains=query) | Q(description__icontains=query))    
    return render(request, 'shop/search.html', {'products':products, 'query':'','categories':Category.objects.all()})

def price_filter(request):
    min_price = request.GET.get('min_price')
    max_price = request.GET.get('max_price')

    if min_price == '' or max_price == '':
        return redirect('shop:items')
    
    min_price= (int)(min_price)
    max_price= (int)(max_price)
     
    if min_price > max_price:
        return redirect('shop:items')

    products = Product.objects.filter(price__gte=min_price, price__lte=max_price)
    category = Category.objects.all()
    ctx = {
        'min_price':min_price,
        'max_price':max_price,
        'products':products,
        'categories':category,
    }

    return render(request, 'shop/price_filter.html', ctx)

def category_filter(request,slug):
    category = Category.objects.all()
    products = Product.objects.filter(category__slug=category.get(slug=slug).slug)
    ctx = {
        'categories':category,
        'products':products
    }
    return render(request, 'shop/category_filter.html', ctx)
    
def size_filter(request):
    category = Category.objects.all()
    size_list = request.GET.getlist('size')
    print(size_list)
    products = Product.objects.filter(size__in=size_list)
    print(products)
    ctx = {
        'categories':category,
        'products':products
    }
    return render(request, 'shop/size_filter.html',ctx)