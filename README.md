# Progetto Tecnologie Web
Progetto per la realizzazione di un portale ecommerce per il corso di Tecnologie Web con il framework web Django.
## Requisiti
- Python 3.10 
- pipenv
## Steps
1. Installare le dipendenze:
```pipenv install``` 
2. Attivare l'ambiente virtuale:
```pipenv shell```
3. Attivare il server:
``` python manage.py runserver ```
4. Test del progetto
``` python manage.py test ```

## Credenziali di accesso
Nel progetto è incluso il database db.sqlite3 con alcuni prodotti d'esempio.

- Utente normale:
```
Username: customer
password provaprova
```
- Utente seller:
```
Username: seller
password provaprova

Username: zara
password zarazara

Username: nike
password nikenike

Username: adidas
password adidasadidas
```